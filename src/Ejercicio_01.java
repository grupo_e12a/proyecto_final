import java.util.Arrays;

public class Ejercicio_01
{
    public static String [] alumnos = {"Roxana","Mariana","Daphne","Jennifer","Lilia","Valeria"};
    public static int[] notas = {16, 15, 18, 19, 20, 14};
    public static String [] nombre_completos = {"Roxana Perez","Mariana Alvarez","Daphne Garcia","Jennifer Linares","Lilia Abanto","Valeria Silva"};

    public static void main(String[] args)
    {
        double promedio;

        String [] alumnos_mas_promedio = new String[alumnos.length];

        promedio = calcularPromedio();
        alumnos_mas_promedio = hallarAlumnosMasPromedio(promedio);

        System.out.printf("El promedio del salon es: %.2f\n", promedio);
        System.out.println("Los alumnos que tienen nota mayor al promedio son: " + Arrays.toString(alumnos_mas_promedio));
    }

    public static double calcularPromedio()
    {
        double valor_promedio = 0;

        for(int i = 0; i < notas.length; i++)
        {
            valor_promedio = valor_promedio + notas[i];
        }

        valor_promedio = valor_promedio / notas.length;

        return valor_promedio;
    }

    public static String[] hallarAlumnosMasPromedio(double promedio_salon)
    {
        String [] alumnos_promedio = new String[nombre_completos.length];
        int j = 0;

        for(int i = 0; i < nombre_completos.length; i++)
        {
            if(notas[i] >= promedio_salon)
            {
                alumnos_promedio[j] = nombre_completos[i];
                j++;
            }
        }

        String [] alumnos_promedio_optimo = new String[j];

        for(int i = 0; i < j; i++)
        {
            alumnos_promedio_optimo[i] = alumnos_promedio[i];
        }

        return alumnos_promedio_optimo;
    }
}
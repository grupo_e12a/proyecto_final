import java.util.Arrays;

public class Ejercicio_00
{
    public static void main(String[] args)
    {
        String [] alumnos = {"Roxana","Mariana","Daphne","Jennifer","Lilia","Valeria"};
        int[] notas = {16, 15, 18, 19, 20, 14};

        double promedio;

        String [] alumnos_mas_promedio = new String[alumnos.length];

        promedio = calcularPromedio(notas);
        alumnos_mas_promedio = hallarAlumnosMasPromedio(alumnos, notas, promedio);

        System.out.printf("El promedio del salon es: %.2f\n", promedio);
        System.out.println("Los alumnos que tienen nota mayor al promedio son: " + Arrays.toString(alumnos_mas_promedio));
    }

    public static double calcularPromedio(int[] lista_notas)
    {
        double valor_promedio = 0;

        for(int i = 0; i < lista_notas.length; i++)
        {
            valor_promedio = valor_promedio + lista_notas[i];
        }

        valor_promedio = valor_promedio / lista_notas.length;

        return valor_promedio;
    }

    public static String[] hallarAlumnosMasPromedio(String[] lista_alumnos, int[] lista_notas, double promedio_salon)
    {
        String [] alumnos_promedio = new String[lista_alumnos.length];
        int j = 0;

        for(int i = 0; i < lista_alumnos.length; i++)
        {
            if(lista_notas[i] >= promedio_salon)
            {
                alumnos_promedio[j] = lista_alumnos[i];
                j++;
            }
        }

        String [] alumnos_promedio_optimo = new String[j];

        for(int i = 0; i < j; i++)
        {
            alumnos_promedio_optimo[i] = alumnos_promedio[i];
        }

        return alumnos_promedio_optimo;
    }
}
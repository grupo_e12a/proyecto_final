import java.util.Arrays;

public class Ejercicio_02
{
    public static String[] candidato = {"KF", "KF", "PK", "PK", "AL", "AL", "PR"};
    public static String[] hoja_vida = {"EL", "FA", "CP", "IR", "CP", "IR", "FA"};
    public static int[] puntaje      = {300 ,  100,   50,  140,   80,  330,  100};

    public static void main(String[] args)
    {
        String candidato_buscado = "PK", candidato_mayor;
        int puntaje;
        String [] candidatos_unicos;

        puntaje = calcularPuntaje(candidato_buscado);
        candidatos_unicos = filtrarCandidatosUnicos();

        candidato_mayor = calcularCandidatoMayor(candidatos_unicos);

        System.out.println("\nEl puntaje del candidato '" + candidato_buscado + "' es: " + puntaje);

        System.out.println("Los candidatos unicos son: " + Arrays.toString(candidatos_unicos));

        System.out.println("El candidato con el mayor puntaje es: " + candidato_mayor + " que obtuvo " + calcularPuntaje(candidato_mayor) + " puntos");
    }

    public static int calcularPuntaje(String iniciales_candidato)
    {
        int sumatoria = 0;

        for(int i=0; i < candidato.length; i++)
        {
            if(candidato[i].equals(iniciales_candidato))
            {
                sumatoria = sumatoria + puntaje[i];
            }
        }

        return sumatoria;
    }

    public static String[] filtrarCandidatosUnicos()
    {
        String [] candidatos_filtrados_0 = new String[candidato.length];
        String candidato_anterior = "", candidato_actual = "";
        int j = 0;

        for(int i = 1; i<candidato.length; i++)
        {
            candidato_anterior = candidato[i-1];
            candidato_actual = candidato[i];

            if(!candidato_anterior.equals(candidato_actual))
            {
                candidatos_filtrados_0[j] = candidato_anterior;
                j++;
            }
        }
        candidatos_filtrados_0[j] = candidato_actual;
        j++;

        String [] candidatos_filtrados_1 = new String[j];

        for(int i = 0; i < j; i++)
        {
            candidatos_filtrados_1[i] = candidatos_filtrados_0[i];
        }

        return candidatos_filtrados_1;
    }

    public static String calcularCandidatoMayor(String[] candidatos_unicos)
    {
        String candidato_mayor_puntaje = "";
        int mayor_puntaje = 0, puntaje_actual = 0;
        int indice_candidato_mayor = 0;

        for (int i=0; i<candidatos_unicos.length; i++)
        {
            puntaje_actual = calcularPuntaje(candidatos_unicos[i]);

            if(puntaje_actual > mayor_puntaje)
            {
                mayor_puntaje = puntaje_actual;
                indice_candidato_mayor = i;
            }
        }

        candidato_mayor_puntaje = candidatos_unicos[indice_candidato_mayor];

        return candidato_mayor_puntaje;
    }
}
